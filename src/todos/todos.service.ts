import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo-dto';
import { Todo } from './interfaces/todo.interface';

@Injectable()
export class TodosService {
   todos: Todo[] = [
        {
            id: 1,
            title: 'todos app',
            description: 'Create NestJS todos app',
            done: false,
        },

        {
            id: 2,
            title: 'bread',
            description: 'bay bread for me',
            done: true,
        },

    ];

    findAll(): Todo[] {
        return this.todos;
    }

    create(todo: CreateTodoDto) {
        this.todos = [...this.todos, todo];
    }

    findOne(id: string){
        return this.todos.find(todo => todo.id === Number(id));
    }

    update(id: string, todo: Todo){
        //recup du todo a modifier
      const todoToUpdate = this.todos.find(t => t.id === +id);
      if(!todoToUpdate){
        return new NotFoundException('booo did you find this todo');
      }

      //Valider les modifications
      if(todo.hasOwnProperty('done')) {
          todoToUpdate.done = todo.done;
      }
      if(todo.title){
          todoToUpdate.title = todo.title;
      }
      if(todo.description) {
          todoToUpdate.description = todo.description
      }
       
      const updatedTodos = this.todos.map(t => t.id !== +id ? t : todoToUpdate);
      this.todos = [...updatedTodos];

      //Et on finis par retourner une reponse dont 1 seul update et le corps de la repoonse
      return { updatedTodos: 1, todo: todo};
    }

    delete(id: string) {
        const nbOfTodoBeforeDelete = this.todos.length;
        this.todos = [...this.todos.filter(t => t.id !== +id)];
        if(this.todos.length < nbOfTodoBeforeDelete) {
            return { deletedTodos: 1, nbTodos: this.todos.length };
        } else {
            return { deletedTodos: 0, nbTodos: this.todos.length };
             
        }
    }
     
}
