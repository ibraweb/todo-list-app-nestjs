import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo-dto';
import { Todo } from './interfaces/todo.interface';
import { TodosService } from './todos.service';

//Il vas ecouter localhost:3000/todos
@Controller('todos')
export class TodosController {

    constructor(private readonly todosService: TodosService) {}
    @Get()
    findAll(): Todo[]{
        return this.todosService.findAll();
    }

    @Post()
    createTodo(@Body() newTodo: CreateTodoDto) {

       // console.log('NewTodo', newTodo);
        this.todosService.create(newTodo)
        
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
       // console.log('id', id);
      return this.todosService.findOne(id)
        
    }

    @Patch(':id')
    updateTodo(@Param('id') id: string, @Body() todo: CreateTodoDto) {
          this.todosService.update(id, todo);
    }

    @Delete(':id')
    deleteTodo(@Param('id') id: string) {
        this.todosService.delete(id);

    }

}
